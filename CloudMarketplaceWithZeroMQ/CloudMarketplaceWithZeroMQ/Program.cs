﻿// Copyright ©2015 THEODOROS NIKOLAKOPOULOS
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software
// is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ZeroMQ;
using BaseXClient;

namespace ResolutionWithZeroMQ2
{
    public enum Architectures { SingleQueue, SeparateQueues, NoQueue }

    class Program
    {
        static void Main(string[] args)
        {
            //if (Directory.Exists(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8"))
            //    Directory.Delete(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8", true); // REVIEW
            //--//Console.WriteLine("Deleted previous output."); // REVIEW
            if (!Directory.Exists(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8"))
                Directory.CreateDirectory(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8"); // REVIEW
            foreach (string file in Directory.GetFiles(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8"))
            {
                File.Delete(file);
            }
            //Directory.CreateDirectory(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8"); // REVIEW
            //Directory.CreateDirectory(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\Technical"); // REVIEW
            //Directory.CreateDirectory(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\Business"); // REVIEW
            //Directory.CreateDirectory(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\Price"); // REVIEW
            //Directory.CreateDirectory(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\Resolution"); // REVIEW
            DateTime? startTime = null;

            // Measurement parameters
            Architectures architecture = Architectures.NoQueue;
            int queueSize = 0;
            int technicalDelay = 110;
            int businessDelay = 10;
            int priceAggregatorDelay = 0;
            int resolutionDelay = 40;
            //technicalDelay = businessDelay = priceAggregatorDelay = resolutionDelay = 0;
            int numOfCopies = 5;

            switch (architecture)
            {
                case Architectures.SeparateQueues:
                case Architectures.NoQueue:
                    new Technical("tcp://localhost:49031", "tcp://*:49032", architecture, queueSize / 4, technicalDelay, numOfCopies);
                    new Business("tcp://localhost:49032", "tcp://*:49033", architecture, queueSize / 4, businessDelay);
                    new PriceAggregator("tcp://localhost:49033", "tcp://*:49034", architecture, queueSize / 4, priceAggregatorDelay);
                    new Resolution("tcp://localhost:49034", "tcp://*:49035", architecture, queueSize / 4, resolutionDelay);

                    using (ZContext context = new ZContext())
                    using (ZSocket sendQueue = new ZSocket(context, ZSocketType.DEALER),
                                   receiveQueue = new ZSocket(context, ZSocketType.DEALER))
                    using (ZMessage messageUserRequirements = new ZMessage(),
                                    messageTotalFiles = new ZMessage())
                    {
                        sendQueue.Bind("tcp://*:49031");
                        receiveQueue.Connect("tcp://localhost:49035");
                        messageUserRequirements.Add(new ZFrame(File.ReadAllText(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\1-UserRequirements.xml")));
                        System.Threading.Thread.Sleep(2000);
                        startTime = DateTime.Now;
                        Console.WriteLine("Sending user requirements.");
                        sendQueue.Send(messageUserRequirements);
                        messageTotalFiles.Add(new ZFrame("TotalFiles1"));
                        sendQueue.Send(messageTotalFiles);
                        receiveQueue.ReceiveMessage();
                    }
                    break;
                case Architectures.SingleQueue:
                    new SingleQueue("tcp://*:49021", "tcp://*:49022", queueSize);
                    new Technical("tcp://localhost:49022", "tcp://localhost:49021", architecture, queueSize, technicalDelay, numOfCopies);
                    new Business("tcp://localhost:49022", "tcp://localhost:49021", architecture, queueSize, businessDelay);
                    new PriceAggregator("tcp://localhost:49022", "tcp://localhost:49021", architecture, queueSize, priceAggregatorDelay);
                    new Resolution("tcp://localhost:49022", "tcp://localhost:49021", architecture, queueSize, resolutionDelay);

                    using (ZContext context = new ZContext())
                    using (ZSocket sendQueue = new ZSocket(context, ZSocketType.DEALER),
                                   receiveQueue = new ZSocket(context, ZSocketType.DEALER))
                    using (ZMessage messageUserRequirements = new ZMessage(),
                                    messageTotalFiles = new ZMessage())
                    {
                        sendQueue.Connect("tcp://localhost:49021");
                        receiveQueue.IdentityString = "Program";
                        receiveQueue.Connect("tcp://localhost:49022");
                        // Single queue routing initialization
                        //receiveQueue.SendMessage(new ZMessage(new ZFrame[] { new ZFrame("Program") }));
                        messageUserRequirements.Add(new ZFrame("Technical"));
                        messageUserRequirements.Add(new ZFrame(File.ReadAllText(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\1-UserRequirements.xml")));
                        System.Threading.Thread.Sleep(2000);
                        startTime = DateTime.Now;
                        Console.WriteLine("Sending user requirements.");
                        sendQueue.Send(messageUserRequirements);
                        messageTotalFiles.Add(new ZFrame("Technical"));
                        messageTotalFiles.Add(new ZFrame("TotalFiles1"));
                        sendQueue.Send(messageTotalFiles);
                        receiveQueue.ReceiveMessage();
                    }
                    break;
            }
            File.WriteAllText(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\" + (DateTime.Now - (DateTime)startTime).TotalSeconds.ToString().Replace('.', ',') + ".txt", "");
            //Console.Read();
        }
    }

    public class Technical : ResolutionBase
    {
        public int NumOfCopies { get; private set; }

        public Technical(string receiveQueueAddress, string sendQueueAddress, Architectures architecture, int rcvHwm, int arraySize, int numOfCopies)
            : base(receiveQueueAddress, sendQueueAddress, architecture, rcvHwm, arraySize)
        {
            NumOfCopies = numOfCopies;
            this.backgroundWorker.DoWork += backgroundWorker_DoWork;
            this.backgroundWorker.RunWorkerAsync();
        }
        public Technical(string receiveQueueAddress, string sendQueueAddress, Architectures architecture, int rcvHwm, int arraySize)
            : this(receiveQueueAddress, sendQueueAddress, architecture, rcvHwm, arraySize, 1) { }

        protected override int CreateAndSendFiles(XmlDocument xmlIn, ZSocket sendQueue)
        {
            if (xmlIn == null)
                return 0;
            // Parse user requirements.
            string databaseType = xmlIn.SelectSingleNode("/XML/UserRequirements/Service/Database/Type").InnerText;
            string databaseOp = xmlIn.SelectSingleNode("/XML/UserRequirements/Service/Database/OP").InnerText;
            string operationSystemType = xmlIn.SelectSingleNode("/XML/UserRequirements/Service/OperationSystem/Type").InnerText;
            string operationSystemOp = xmlIn.SelectSingleNode("/XML/UserRequirements/Service/OperationSystem/OP").InnerText;
            string crmOp = xmlIn.SelectSingleNode("/XML/UserRequirements/Service/CRM/OP").InnerText;
            string networkType = xmlIn.SelectSingleNode("/XML/UserRequirements/Service/Network/Type").InnerText;

            // Query technical blueprints database.
            Session session = new Session("localhost", 1984, "admin", "admin");
            session.Execute("open TechnicalBlueprints");
            string queryString;
            XmlDocument databases = new XmlDocument();
            XmlDocument operationSystems = new XmlDocument();
            XmlDocument crms = new XmlDocument();
            XmlDocument networks = new XmlDocument();
            // Databases
            queryString = "<Databases>{/TechnicalBlueprints/Database";
            if (!String.IsNullOrWhiteSpace(databaseType)
                || !String.IsNullOrWhiteSpace(databaseOp))
            {
                queryString += "[";
                if (!String.IsNullOrWhiteSpace(databaseType))
                    queryString += "Type = \"" + databaseType.Trim() + "\" and ";
                if (!String.IsNullOrWhiteSpace(databaseOp))
                    queryString += "OP = \"" + databaseOp.Trim() + "\" and ";
                queryString = queryString.Substring(0, queryString.Length - 5) + "]";
            }
            queryString += "}</Databases>";
            databases.LoadXml(session.Query(queryString).Execute());
            // Operation Systems
            queryString = "<OperationSystems>{/TechnicalBlueprints/OperationSystem";
            if (!String.IsNullOrWhiteSpace(operationSystemType)
                || !String.IsNullOrWhiteSpace(operationSystemOp))
            {
                queryString += "[";
                if (!String.IsNullOrWhiteSpace(operationSystemType))
                    queryString += "Type = \"" + operationSystemType.Trim() + "\" and ";
                if (!String.IsNullOrWhiteSpace(operationSystemOp))
                    queryString += "OP = \"" + operationSystemOp.Trim() + "\" and ";
                queryString = queryString.Substring(0, queryString.Length - 5) + "]";
            }
            queryString += "}</OperationSystems>";
            operationSystems.LoadXml(session.Query(queryString).Execute());
            // CRMs
            queryString = "<CRMs>{/TechnicalBlueprints/CRM";
            if (!String.IsNullOrWhiteSpace(crmOp))
                queryString += "[OP = \"" + crmOp.Trim() + "\"]";
            queryString += "}</CRMs>";
            crms.LoadXml(session.Query(queryString).Execute());
            // Networks
            queryString = "<Networks>{/TechnicalBlueprints/Network";
            if (!String.IsNullOrWhiteSpace(networkType))
                queryString += "[Type = \"" + networkType.Trim() + "\"]";
            queryString += "}</Networks>";
            networks.LoadXml(session.Query(queryString).Execute());
            session.Close();

            // Create and send technical resolutions.
            int filesSent = 0;
            for (int i = 0; i < NumOfCopies; i++)
                foreach (XmlNode database in databases.SelectNodes("/Databases/Database"))
                    foreach (XmlNode operationSystem in operationSystems.SelectNodes("/OperationSystems/OperationSystem"))
                        foreach (XmlNode crm in crms.SelectNodes("/CRMs/CRM"))
                            foreach (XmlNode network in networks.SelectNodes("/Networks/Network"))
                            {
                                // Create one XML.
                                XmlDocument xmlOut = new XmlDocument();
                                xmlOut.LoadXml(xmlIn.OuterXml);
                                XmlNode node = xmlOut.SelectSingleNode("/XML/Technical");
                                node.AppendChild(xmlOut.ImportNode(database, true));
                                node.AppendChild(xmlOut.ImportNode(operationSystem, true));
                                node.AppendChild(xmlOut.ImportNode(crm, true));
                                node.AppendChild(xmlOut.ImportNode(network, true));
                                // Delay
                                Delay();
                                // Send one XML.
                                if (Architecture != Architectures.NoQueue)
                                    using (ZMessage message = new ZMessage())
                                    {
                                        switch (Architecture)
                                        {
                                            case Architectures.SeparateQueues:
                                                message.Add(new ZFrame(xmlOut.OuterXml));
                                                break;
                                            case Architectures.SingleQueue:
                                                message.Add(new ZFrame("Business"));
                                                message.Add(new ZFrame(xmlOut.OuterXml));
                                                break;
                                        }
                                        sendQueue.Send(message);
                                    }
                                else
                                    this.filesToBeSentList.Add(xmlOut.OuterXml);
                                filesSent++;
                                //--//Console.WriteLine("  Technical: " + filesSent);
                            }
            return filesSent;
        }
        protected void Delay()
        {
            if (ArraySize <= 0)
                return;
            int d1 = ArraySize, d2 = ArraySize, d3 = ArraySize;
            int[,] factor1 = new int[d1, d2];
            int[,] factor2 = new int[d2, d3];
            int[,] product = new int[d1, d3];
            Random random = new Random();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor1.GetLength(1); j++)
                    factor1[i, j] = random.Next();
            for (int i = 0; i < factor2.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                    factor2[i, j] = random.Next();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                {
                    product[i, j] = 0;
                    for (int k = 0; k < d2; k++)
                        product[i, j] += factor1[i, k] * factor2[k, j];
                }
            return;
        }
        protected void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            using (ZContext context = new ZContext())
            using (ZSocket receiveQueue = new ZSocket(context, ZSocketType.DEALER),
                           sendQueue = new ZSocket(context, ZSocketType.DEALER))
            {
                int filesToBeReceived = 0;
                // Initialize queues
                sendQueue.SendHighWatermark = 2;
                if (!String.IsNullOrWhiteSpace(SendQueueAddress))
                    switch (Architecture)
                    {
                        case Architectures.SeparateQueues:
                        case Architectures.NoQueue:
                            receiveQueue.ReceiveHighWatermark = RcvHwm;
                            sendQueue.Bind(SendQueueAddress);
                            break;
                        case Architectures.SingleQueue:
                            sendQueue.Connect(SendQueueAddress);
                            receiveQueue.ReceiveHighWatermark = 2;
                            receiveQueue.IdentityString = this.GetType().Name;
                            break;
                    }
                receiveQueue.Connect(ReceiveQueueAddress);
                Console.WriteLine(this.GetType().Name + " ready.");
                while (true)
                {
                    // Receive message.
                    string messageString;
                    using (ZFrame frame = receiveQueue.ReceiveMessage()[0])
                    {
                        messageString = frame.ReadString();
                    }
                    if (messageString.StartsWith("TotalFiles"))
                        filesToBeReceived = Int32.Parse(messageString.Substring("TotalFiles".Length));
                    else
                    {
                        XmlDocument xmlIn = new XmlDocument();
                        xmlIn.LoadXml(messageString);
                        FilesReceived++;
                        FilesSent += CreateAndSendFiles(xmlIn, sendQueue);
                        //--//Console.WriteLine("    Business: " + sentFiles + "/" + processedFiles);
                    }
                    if (FilesReceived == filesToBeReceived)
                    {
                        if (Architecture == Architectures.NoQueue)
                            foreach (string xmlOut in this.filesToBeSentList)
                                using (ZFrame frame = new ZFrame(xmlOut))
                                using (ZMessage message = new ZMessage(new ZFrame[] { frame }))
                                {
                                    sendQueue.SendMessage(message);
                                }
                        ZMessage messageTotalFiles = new ZMessage();
                        if (Architecture == Architectures.SingleQueue)
                            switch (this.GetType().Name)
                            {
                                case "Technical":
                                    messageTotalFiles.Add(new ZFrame("Business"));
                                    break;
                                case "Business":
                                    messageTotalFiles.Add(new ZFrame("PriceAggregator"));
                                    break;
                                case "PriceAggregator":
                                    messageTotalFiles.Add(new ZFrame("Resolution"));
                                    break;
                                case "Resolution":
                                    messageTotalFiles.Add(new ZFrame("Program"));
                                    break;
                            }
                        messageTotalFiles.Add(new ZFrame("TotalFiles" + FilesSent));
                        sendQueue.SendMessage(messageTotalFiles);
                        break;
                    }
                }
                Console.WriteLine(this.GetType().Name + ": " + filesToBeReceived + " -> " + FilesSent);
            }

        }
    }

    public class Business : ResolutionBase
    {
        public Business(string receiveQueueAddress, string sendQueueAddress, Architectures architecture, int rcvHwm, int arraySize)
            : base(receiveQueueAddress, sendQueueAddress, architecture, rcvHwm, arraySize)
        {
            this.backgroundWorker.DoWork += backgroundWorker_DoWork;
            this.backgroundWorker.RunWorkerAsync();
        }


        protected override int CreateAndSendFiles(XmlDocument xmlIn, ZSocket sendQueue)
        {
            if (xmlIn == null)
                return 0;
            // Parse technical resolution.
            string databaseId = xmlIn.SelectSingleNode("/XML/Technical/Database/ID").InnerText;
            string operationSystemId = xmlIn.SelectSingleNode("/XML/Technical/OperationSystem/ID").InnerText;
            string crmId = xmlIn.SelectSingleNode("/XML/Technical/CRM/ID").InnerText;
            string networkId = xmlIn.SelectSingleNode("/XML/Technical/Network/ID").InnerText;

            // Query business bleuprints database.
            Session session = new Session("localhost", 1984, "admin", "admin");
            session.Execute("open BusinessBlueprints");
            string queryString;
            XmlDocument pDatabases = new XmlDocument();
            XmlDocument pOperationSystems = new XmlDocument();
            XmlDocument pCrms = new XmlDocument();
            XmlDocument pNetworks = new XmlDocument();
            // Databases
            queryString = "<PDatabases>{BusinessBlueprints/PDatabase";
            if (!String.IsNullOrWhiteSpace(databaseId))
                queryString += "[ID = " + databaseId.Trim() + "]";
            queryString += "}</PDatabases>";
            pDatabases.LoadXml(session.Query(queryString).Execute());
            // OperationSystems
            queryString = "<POperationSystems>{BusinessBlueprints/POperationSystem";
            if (!String.IsNullOrWhiteSpace(operationSystemId))
                queryString += "[ID = " + operationSystemId.Trim() + "]";
            queryString += "}</POperationSystems>";
            pOperationSystems.LoadXml(session.Query(queryString).Execute());
            // CRMs
            queryString = "<PCrms>{BusinessBlueprints/PCRM";
            if (!String.IsNullOrWhiteSpace(crmId))
                queryString += "[ID = " + crmId.Trim() + "]";
            queryString += "}</PCrms>";
            pCrms.LoadXml(session.Query(queryString).Execute());
            // Networks
            queryString = "<PNetworks>{BusinessBlueprints/PNetwork";
            if (!String.IsNullOrWhiteSpace(networkId))
                queryString += "[ID = " + networkId.Trim() + "]";
            queryString += "}</PNetworks>";
            pNetworks.LoadXml(session.Query(queryString).Execute());

            // Create and send business resolutions.
            int filesSent = 0;
            foreach (XmlNode pDatabase in pDatabases.SelectNodes("/PDatabases/PDatabase"))
                foreach (XmlNode pOperationSystem in pOperationSystems.SelectNodes("/POperationSystems/POperationSystem"))
                    foreach (XmlNode pCrm in pCrms.SelectNodes("/PCrms/PCRM"))
                        foreach (XmlNode pNetwork in pNetworks.SelectNodes("/PNetworks/PNetwork"))
                        {
                            // Create one XML.
                            XmlDocument xmlOut = new XmlDocument();
                            xmlOut.LoadXml(xmlIn.OuterXml);
                            XmlNode node = xmlOut.SelectSingleNode("/XML/Business");
                            node.AppendChild(xmlOut.ImportNode(pDatabase, true));
                            node.AppendChild(xmlOut.ImportNode(pOperationSystem, true));
                            node.AppendChild(xmlOut.ImportNode(pCrm, true));
                            node.AppendChild(xmlOut.ImportNode(pNetwork, true));
                            //Delay
                            Delay();
                            // Send one XML
                            if (Architecture != Architectures.NoQueue)
                                using (ZMessage message = new ZMessage())
                                {
                                    switch (Architecture)
                                    {
                                        case Architectures.SeparateQueues:
                                            message.Add(new ZFrame(xmlOut.OuterXml));
                                            break;
                                        case Architectures.SingleQueue:
                                            message.Add(new ZFrame("PriceAggregator"));
                                            message.Add(new ZFrame(xmlOut.OuterXml));
                                            break;
                                    }
                                    sendQueue.Send(message);
                                }
                            else
                                this.filesToBeSentList.Add(xmlOut.OuterXml);
                            filesSent++;
                            //xmlOut.Save(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\Business\" + filesSent + "-" + new Random().Next(1000000) + ".xml"); // REVIEW
                        }
            return filesSent;
        }
        protected void Delay()
        {
            if (ArraySize <= 0)
                return;
            int d1 = ArraySize, d2 = ArraySize, d3 = ArraySize;
            int[,] factor1 = new int[d1, d2];
            int[,] factor2 = new int[d2, d3];
            int[,] product = new int[d1, d3];
            Random random = new Random();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor1.GetLength(1); j++)
                    factor1[i, j] = random.Next();
            for (int i = 0; i < factor2.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                    factor2[i, j] = random.Next();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                {
                    product[i, j] = 0;
                    for (int k = 0; k < d2; k++)
                        product[i, j] += factor1[i, k] * factor2[k, j];
                }
            return;
        }
        protected void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            using (ZContext context = new ZContext())
            using (ZSocket receiveQueue = new ZSocket(context, ZSocketType.DEALER),
                           sendQueue = new ZSocket(context, ZSocketType.DEALER))
            {
                int filesToBeReceived = 0;
                // Initialize queues
                sendQueue.SendHighWatermark = 2;
                if (!String.IsNullOrWhiteSpace(SendQueueAddress))
                    switch (Architecture)
                    {
                        case Architectures.SeparateQueues:
                        case Architectures.NoQueue:
                            receiveQueue.ReceiveHighWatermark = RcvHwm;
                            sendQueue.Bind(SendQueueAddress);
                            break;
                        case Architectures.SingleQueue:
                            sendQueue.Connect(SendQueueAddress);
                            receiveQueue.ReceiveHighWatermark = 2;
                            receiveQueue.IdentityString = this.GetType().Name;
                            break;
                    }
                receiveQueue.Connect(ReceiveQueueAddress);
                Console.WriteLine(this.GetType().Name + " ready.");
                while (true)
                {
                    // Receive message.
                    string messageString;
                    using (ZFrame frame = receiveQueue.ReceiveMessage()[0])
                    {
                        messageString = frame.ReadString();
                    }
                    if (messageString.StartsWith("TotalFiles"))
                        filesToBeReceived = Int32.Parse(messageString.Substring("TotalFiles".Length));
                    else
                    {
                        XmlDocument xmlIn = new XmlDocument();
                        xmlIn.LoadXml(messageString);
                        FilesReceived++;
                        FilesSent += CreateAndSendFiles(xmlIn, sendQueue);
                        //--//Console.WriteLine("    Business: " + sentFiles + "/" + processedFiles);
                    }
                    if (FilesReceived == filesToBeReceived)
                    {
                        if (Architecture == Architectures.NoQueue)
                            foreach (string xmlOut in this.filesToBeSentList)
                                using (ZFrame frame = new ZFrame(xmlOut))
                                using (ZMessage message = new ZMessage(new ZFrame[] { frame }))
                                {
                                    sendQueue.SendMessage(message);
                                }
                        ZMessage messageTotalFiles = new ZMessage();
                        if (Architecture == Architectures.SingleQueue)
                            switch (this.GetType().Name)
                            {
                                case "Technical":
                                    messageTotalFiles.Add(new ZFrame("Business"));
                                    break;
                                case "Business":
                                    messageTotalFiles.Add(new ZFrame("PriceAggregator"));
                                    break;
                                case "PriceAggregator":
                                    messageTotalFiles.Add(new ZFrame("Resolution"));
                                    break;
                                case "Resolution":
                                    messageTotalFiles.Add(new ZFrame("Program"));
                                    break;
                            }
                        messageTotalFiles.Add(new ZFrame("TotalFiles" + FilesSent));
                        sendQueue.SendMessage(messageTotalFiles);
                        break;
                    }
                }
                Console.WriteLine(this.GetType().Name + ": " + filesToBeReceived + " -> " + FilesSent);
            }

        }
    }

    public class PriceAggregator : ResolutionBase
    {
        public PriceAggregator(string receiveQueueAddress, string sendQueueAddress, Architectures architecture, int rcvHwm, int arraySize)
            : base(receiveQueueAddress, sendQueueAddress, architecture, rcvHwm, arraySize)
        {
            this.backgroundWorker.DoWork += backgroundWorker_DoWork;
            this.backgroundWorker.RunWorkerAsync();
        }
        protected override int CreateAndSendFiles(XmlDocument xmlIn, ZSocket sendQueue)
        {
            if (xmlIn == null)
                return 0;
            // Delay
            Delay();
            int databasePrice, operationSystemPrice, crmPrice, networkPrice, maxPrice = 0;
            if (!Int32.TryParse(xmlIn.SelectSingleNode("/XML/Business/PDatabase/Price").InnerText, out databasePrice)
                || !Int32.TryParse(xmlIn.SelectSingleNode("/XML/Business/POperationSystem/Price").InnerText, out operationSystemPrice)
                || !Int32.TryParse(xmlIn.SelectSingleNode("/XML/Business/PCRM/Price").InnerText, out crmPrice)
                || !Int32.TryParse(xmlIn.SelectSingleNode("/XML/Business/PNetwork/Price").InnerText, out networkPrice))
                return 0; // If a price is missing or cannot be parsed, do not proceed. REVIEW
            int totalPrice = databasePrice + operationSystemPrice + crmPrice + networkPrice;
            if ((xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/MAXPrice") != null)
                && (!Int32.TryParse(xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/MAXPrice").InnerText, out maxPrice)
                    || (totalPrice > maxPrice)))
                return 0; // If the total price of the resolution is higher than the user specified price, reject it.

            // Create and send price resolution.
            XmlDocument xmlOut = new XmlDocument();
            xmlOut.LoadXml(xmlIn.OuterXml);
            XmlElement totalPriceNode = xmlOut.CreateElement("TotalPrice");
            totalPriceNode.InnerText = totalPrice.ToString();
            xmlOut.SelectSingleNode("/XML").AppendChild(totalPriceNode);
            if (Architecture != Architectures.NoQueue)
                using (ZMessage message = new ZMessage())
                {
                    switch (Architecture)
                    {
                        case Architectures.SeparateQueues:
                            message.Add(new ZFrame(xmlOut.OuterXml));
                            break;
                        case Architectures.SingleQueue:
                            message.Add(new ZFrame("Resolution"));
                            message.Add(new ZFrame(xmlOut.OuterXml));
                            break;
                    }
                    sendQueue.Send(message);
                }
            else
                this.filesToBeSentList.Add(xmlOut.OuterXml);
            //xmlOut.Save(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\Price\" + counter1++ + ".xml"); // REVIEW
            ////--//Console.WriteLine(counter2); // REVIEW
            return 1;
        }
        protected void Delay()
        {
            if (ArraySize <= 0)
                return;
            int d1 = ArraySize, d2 = ArraySize, d3 = ArraySize;
            int[,] factor1 = new int[d1, d2];
            int[,] factor2 = new int[d2, d3];
            int[,] product = new int[d1, d3];
            Random random = new Random();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor1.GetLength(1); j++)
                    factor1[i, j] = random.Next();
            for (int i = 0; i < factor2.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                    factor2[i, j] = random.Next();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                {
                    product[i, j] = 0;
                    for (int k = 0; k < d2; k++)
                        product[i, j] += factor1[i, k] * factor2[k, j];
                }
            return;
        }
        protected void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            using (ZContext context = new ZContext())
            using (ZSocket receiveQueue = new ZSocket(context, ZSocketType.DEALER),
                           sendQueue = new ZSocket(context, ZSocketType.DEALER))
            {
                int filesToBeReceived = 0;
                // Initialize queues
                sendQueue.SendHighWatermark = 2;
                if (!String.IsNullOrWhiteSpace(SendQueueAddress))
                    switch (Architecture)
                    {
                        case Architectures.SeparateQueues:
                        case Architectures.NoQueue:
                            receiveQueue.ReceiveHighWatermark = RcvHwm;
                            sendQueue.Bind(SendQueueAddress);
                            break;
                        case Architectures.SingleQueue:
                            sendQueue.Connect(SendQueueAddress);
                            receiveQueue.ReceiveHighWatermark = 2;
                            receiveQueue.IdentityString = this.GetType().Name;
                            break;
                    }
                receiveQueue.Connect(ReceiveQueueAddress);
                Console.WriteLine(this.GetType().Name + " ready.");
                while (true)
                {
                    // Receive message.
                    string messageString;
                    using (ZFrame frame = receiveQueue.ReceiveMessage()[0])
                    {
                        messageString = frame.ReadString();
                    }
                    if (messageString.StartsWith("TotalFiles"))
                        filesToBeReceived = Int32.Parse(messageString.Substring("TotalFiles".Length));
                    else
                    {
                        XmlDocument xmlIn = new XmlDocument();
                        xmlIn.LoadXml(messageString);
                        FilesReceived++;
                        FilesSent += CreateAndSendFiles(xmlIn, sendQueue);
                        //--//Console.WriteLine("    Business: " + sentFiles + "/" + processedFiles);
                    }
                    if (FilesReceived == filesToBeReceived)
                    {
                        if (Architecture == Architectures.NoQueue)
                            foreach (string xmlOut in this.filesToBeSentList)
                                using (ZFrame frame = new ZFrame(xmlOut))
                                using (ZMessage message = new ZMessage(new ZFrame[] { frame }))
                                {
                                    sendQueue.SendMessage(message);
                                }
                        ZMessage messageTotalFiles = new ZMessage();
                        if (Architecture == Architectures.SingleQueue)
                            switch (this.GetType().Name)
                            {
                                case "Technical":
                                    messageTotalFiles.Add(new ZFrame("Business"));
                                    break;
                                case "Business":
                                    messageTotalFiles.Add(new ZFrame("PriceAggregator"));
                                    break;
                                case "PriceAggregator":
                                    messageTotalFiles.Add(new ZFrame("Resolution"));
                                    break;
                                case "Resolution":
                                    messageTotalFiles.Add(new ZFrame("Program"));
                                    break;
                            }
                        messageTotalFiles.Add(new ZFrame("TotalFiles" + FilesSent));
                        sendQueue.SendMessage(messageTotalFiles);
                        break;
                    }
                }
                Console.WriteLine(this.GetType().Name + ": " + filesToBeReceived + " -> " + FilesSent);
            }
        }
    }

    public class Resolution : ResolutionBase
    {
        private XmlDocument bestResolution;
        private int bestTotal;
        public Resolution(string receiveQueueAddress, string sendQueueAddress, Architectures architecture, int rcvHwm, int arraySize)
            : base(receiveQueueAddress, sendQueueAddress, architecture, rcvHwm, arraySize)
        {
            this.backgroundWorker.DoWork += backgroundWorker_DoWork;
            this.backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;
            this.bestTotal = Int32.MinValue;
            this.backgroundWorker.RunWorkerAsync();
        }

        protected override int CreateAndSendFiles(XmlDocument xmlIn, ZSocket sendQueue)
        {
            // Delay
            Delay();
            // Check optimization requirements.
            int minAvailability = 0, minSecurity = 0, minReputation = 0, minReliability = 0;
            int avgAvailability = 0, avgSecurity = 0, avgReputation = 0, avgReliability = 0, avgConfidentiality = 0;
            bool requestedConfidentiality = false;
            Int32.TryParse(xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/MINAvailability").InnerText, out minAvailability);
            Int32.TryParse(xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/MINSecurity").InnerText, out minSecurity);
            Int32.TryParse(xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/MINReputation").InnerText, out minReputation);
            Int32.TryParse(xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/MINReliability").InnerText, out minReliability);
            if ((xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/Confidentiality") != null)
                && (xmlIn.SelectSingleNode("/XML/UserRequirements/Optimization/Confidentiality").InnerText.Trim().ToUpper() == "YES"))
                requestedConfidentiality = true;
            bool constraintsMet = true;
            XmlNodeList nodeList;
            // Check availability requirements.
            nodeList = xmlIn.SelectNodes("/XML/Business//Availability");
            foreach (XmlNode node in nodeList)
            {
                int availability;
                if (!Int32.TryParse(node.InnerText, out availability)
                    || (availability < minAvailability))
                {
                    constraintsMet = false;
                    break;
                }
                avgAvailability += availability / nodeList.Count;
            }
            if (!constraintsMet)
                return 0; // If any constraint is not met, ignore the resolution.
            // Check security requirements.
            nodeList = xmlIn.SelectNodes("/XML/Business//Security");
            foreach (XmlNode node in nodeList)
            {
                int security;
                if (!Int32.TryParse(node.InnerText, out security)
                    || (security < minSecurity))
                {
                    constraintsMet = false;
                    break;
                }
                avgSecurity += security / nodeList.Count;
            }
            if (!constraintsMet)
                return 0; // If any constraint is not met, ignore the resolution.
            // Check reputation requirements.
            nodeList = xmlIn.SelectNodes("/XML/Business//Reputation");
            foreach (XmlNode node in nodeList)
            {
                int reputation;
                if (!Int32.TryParse(node.InnerText, out reputation)
                    || (reputation < minReputation))
                {
                    constraintsMet = false;
                    break;
                }
                avgReputation += reputation / nodeList.Count;
            }
            if (!constraintsMet)
                return 0; // If any constraint is not met, ignore the resolution.
            // Check reliability requirements.
            nodeList = xmlIn.SelectNodes("/XML/Business//Reliability");
            foreach (XmlNode node in nodeList)
            {
                int reliability;
                if (!Int32.TryParse(node.InnerText, out reliability)
                    || (reliability < minReliability))
                {
                    constraintsMet = false;
                    break;
                }
                avgReliability += reliability / nodeList.Count;
            }
            if (!constraintsMet)
                return 0; // If any constraint is not met, ignore the resolution.
            // Check confidentiality requirements
            nodeList = xmlIn.SelectNodes("/XML/Business//Confidentiality");
            foreach (XmlNode node in nodeList)
            {
                if (requestedConfidentiality
                    && (node.InnerText.Trim().ToUpper() != "YES"))
                {
                    constraintsMet = false;
                    break;
                }
                if (node.InnerText.Trim().ToUpper() == "YES")
                    avgConfidentiality++;
            }
            if (!constraintsMet)
                return 0; // If any constraint is not met, ignore the resolution.

            // Overall evaluation
            int totalPrice;
            if (!Int32.TryParse(xmlIn.SelectSingleNode("/XML/TotalPrice").InnerText, out totalPrice))
                return 0;
            int total = 2000 * (avgAvailability / 10 + avgSecurity + avgReputation + avgReliability / 2 + avgConfidentiality / 2) - totalPrice;
            if (total <= this.bestTotal)
                return 0; // If the current resolution isn't any better, discard it

            XmlDocument resolution = new XmlDocument();
            resolution.LoadXml(xmlIn.OuterXml);
            // Add info to the resolution
            XmlElement element;
            // Availability
            element = resolution.CreateElement("TotalAvailability");
            element.InnerText = avgAvailability.ToString();
            resolution.SelectSingleNode("/XML").AppendChild(element);
            // Security
            element = resolution.CreateElement("TotalSecurity");
            element.InnerText = avgSecurity.ToString();
            resolution.SelectSingleNode("/XML").AppendChild(element);
            // Reputation
            element = resolution.CreateElement("TotalReputation");
            element.InnerText = avgReputation.ToString();
            resolution.SelectSingleNode("/XML").AppendChild(element);
            // Reliability
            element = resolution.CreateElement("TotalReliability");
            element.InnerText = avgReliability.ToString();
            resolution.SelectSingleNode("/XML").AppendChild(element);
            // Confidentiality
            element = resolution.CreateElement("TotalConfidentiality");
            element.InnerText = avgConfidentiality.ToString();
            resolution.SelectSingleNode("/XML").AppendChild(element);
            // Reposition TotalPrice
            resolution.SelectSingleNode("/XML").AppendChild(resolution.SelectSingleNode("/XML/TotalPrice"));
            // TotalTotal
            element = resolution.CreateElement("TotalTotal");
            element.InnerText = total.ToString();
            resolution.SelectSingleNode("/XML").AppendChild(element);
            int returnValue = (this.bestResolution == null) ? 1 : 0;
            this.bestTotal = total;
            this.bestResolution = resolution;
            return returnValue;
        }
        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.bestResolution == null)
                return;
            this.bestResolution.Save(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\" + this.bestTotal + ".xml");
        }
        protected void Delay()
        {
            if (ArraySize <= 0)
                return;
            int d1 = ArraySize, d2 = ArraySize, d3 = ArraySize;
            int[,] factor1 = new int[d1, d2];
            int[,] factor2 = new int[d2, d3];
            int[,] product = new int[d1, d3];
            Random random = new Random();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor1.GetLength(1); j++)
                    factor1[i, j] = random.Next();
            for (int i = 0; i < factor2.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                    factor2[i, j] = random.Next();
            for (int i = 0; i < factor1.GetLength(0); i++)
                for (int j = 0; j < factor2.GetLength(1); j++)
                {
                    product[i, j] = 0;
                    for (int k = 0; k < d2; k++)
                        product[i, j] += factor1[i, k] * factor2[k, j];
                }
            return;
        }
        protected void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            using (ZContext context = new ZContext())
            using (ZSocket receiveQueue = new ZSocket(context, ZSocketType.DEALER),
                           sendQueue = new ZSocket(context, ZSocketType.DEALER))
            {
                int filesToBeReceived = 0;
                // Initialize queues
                sendQueue.SendHighWatermark = 2;
                if (!String.IsNullOrWhiteSpace(SendQueueAddress))
                    switch (Architecture)
                    {
                        case Architectures.SeparateQueues:
                        case Architectures.NoQueue:
                            receiveQueue.ReceiveHighWatermark = RcvHwm;
                            sendQueue.Bind(SendQueueAddress);
                            break;
                        case Architectures.SingleQueue:
                            sendQueue.Connect(SendQueueAddress);
                            receiveQueue.ReceiveHighWatermark = 2;
                            receiveQueue.IdentityString = this.GetType().Name;
                            break;
                    }
                receiveQueue.Connect(ReceiveQueueAddress);
                Console.WriteLine(this.GetType().Name + " ready.");
                while (true)
                {
                    // Receive message.
                    string messageString;
                    using (ZFrame frame = receiveQueue.ReceiveMessage()[0])
                    {
                        messageString = frame.ReadString();
                    }
                    if (messageString.StartsWith("TotalFiles"))
                        filesToBeReceived = Int32.Parse(messageString.Substring("TotalFiles".Length));
                    else
                    {
                        XmlDocument xmlIn = new XmlDocument();
                        xmlIn.LoadXml(messageString);
                        FilesReceived++;
                        FilesSent += CreateAndSendFiles(xmlIn, sendQueue);
                        //--//Console.WriteLine("    Business: " + sentFiles + "/" + processedFiles);
                    }
                    if (FilesReceived == filesToBeReceived)
                    {
                        if (Architecture == Architectures.NoQueue)
                            foreach (string xmlOut in this.filesToBeSentList)
                                using (ZFrame frame = new ZFrame(xmlOut))
                                using (ZMessage message = new ZMessage(new ZFrame[] { frame }))
                                {
                                    sendQueue.SendMessage(message);
                                }
                        ZMessage messageTotalFiles = new ZMessage();
                        if (Architecture == Architectures.SingleQueue)
                            switch (this.GetType().Name)
                            {
                                case "Technical":
                                    messageTotalFiles.Add(new ZFrame("Business"));
                                    break;
                                case "Business":
                                    messageTotalFiles.Add(new ZFrame("PriceAggregator"));
                                    break;
                                case "PriceAggregator":
                                    messageTotalFiles.Add(new ZFrame("Resolution"));
                                    break;
                                case "Resolution":
                                    messageTotalFiles.Add(new ZFrame("Program"));
                                    break;
                            }
                        messageTotalFiles.Add(new ZFrame("TotalFiles" + FilesSent));
                        if (this.bestResolution == null)
                            File.WriteAllText(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\NoResolution.txt", "No resolution could be found for the specified user requirements.");
                        else
                            this.bestResolution.Save(@"C:\Users\Theodore\Documents\Διπλωματική\Κώδικας\Έξοδος\Test8\" + this.bestTotal + ".xml");
                        sendQueue.SendMessage(messageTotalFiles);
                        break;
                    }
                }
                Console.WriteLine(this.GetType().Name + ": " + filesToBeReceived + " -> " + FilesSent);
            }
        }
    }

    public class SingleQueue
    {
        public string ReceiveQueueAddress { get; private set; }
        public string SendQueueAddress { get; private set; }
        public int RcvHwm { get; private set; }
        private BackgroundWorker backgroundWorker;
        public SingleQueue(string receiveQueueAddress, string sendQueueAddress, int rcvHwm)
        {
            ReceiveQueueAddress = receiveQueueAddress;
            SendQueueAddress = sendQueueAddress;
            RcvHwm = rcvHwm;
            this.backgroundWorker = new BackgroundWorker();
            this.backgroundWorker.DoWork += backgroundWorker_DoWork;
            this.backgroundWorker.RunWorkerAsync();
        }
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            using (ZContext context = new ZContext())
            using (ZSocket receiveQueue = new ZSocket(context, ZSocketType.DEALER),
                           sendQueue = new ZSocket(context, ZSocketType.ROUTER))
            {
                receiveQueue.ReceiveHighWatermark = RcvHwm;
                receiveQueue.Bind(ReceiveQueueAddress);
                sendQueue.SendHighWatermark = 2;
                sendQueue.RouterMandatory = RouterMandatory.Report;
                sendQueue.Bind(SendQueueAddress);
                //ZContext.Proxy(sendQueue, receiveQueue);
                while (true)
                    sendQueue.SendMessage(receiveQueue.ReceiveMessage());
            }
        }

    }

    public abstract class ResolutionBase
    {
        public string ReceiveQueueAddress { get; private set; }
        public string SendQueueAddress { get; private set; }
        public Architectures Architecture { get; private set; }
        public int RcvHwm { get; private set; }
        public int ArraySize { get; set; }
        protected List<string> filesToBeSentList;
        protected int FilesReceived;
        //{
        //    get
        //  {
        //      return this.filesReceived;
        //  }
        //    set
        //    {
        //        switch (this.GetType().Name)
        //        {
        //            case "Business":
        //                //Console.WriteLine("    Business has received: " + value);
        //                break;
        //            case "PriceAggregator":
        //                if (value / 1000 > this.filesReceived / 1000)
        //                    Console.WriteLine("    PriceAggregator has received: " + value);
        //                break;
        //            case "Resolution":
        //                if (value / 1000 > this.filesReceived / 1000)
        //                    Console.WriteLine("      Resolution has received: " + value);
        //                break;
        //        }
        //        this.filesReceived = value;
        //    }
        //}
        protected int FilesSent;
        //{
        //    get
        //    {
        //        return this.filesSent;
        //    }
        //    set
        //    {
        //        switch (this.GetType().Name)
        //        {
        //            case "Technical":
        //                Console.WriteLine("  Technical has sent: " + value);
        //                break;
        //            case "Business":
        //                if (value / 1000 > this.filesSent / 1000)
        //                    Console.WriteLine("    Business has sent: " + value);
        //                break;
        //            case "PriceAggregator":
        //                if (value / 1000 > this.filesSent / 1000)
        //                    Console.WriteLine("      PriceAggregator has sent: " + value);
        //                break;
        //        }
        //        this.filesSent = value;
        //    }
        //}
        private int filesReceived;
        private int filesSent;
        protected BackgroundWorker backgroundWorker;

        protected ResolutionBase(string receiveQueueAddress, string sendQueueAddress, Architectures architecture, int rcvHwm, int arraySize)
        {
            ReceiveQueueAddress = receiveQueueAddress;
            SendQueueAddress = sendQueueAddress;
            Architecture = architecture;
            RcvHwm = rcvHwm;
            ArraySize = arraySize;
            FilesReceived = 0;
            FilesSent = 0;
            this.filesToBeSentList = new List<string>();
            this.backgroundWorker = new BackgroundWorker();
        }

        protected abstract int CreateAndSendFiles(XmlDocument xmlIn, ZSocket sendQueue);
        //protected void Delay()
        //{
        //    if (ArraySize <= 0)
        //        return;
        //    int d1 = ArraySize, d2 = ArraySize, d3 = ArraySize;
        //    int[,] factor1 = new int[d1, d2];
        //    int[,] factor2 = new int[d2, d3];
        //    int[,] product = new int[d1, d3];
        //    Random random = new Random();
        //    for (int i = 0; i < factor1.GetLength(0); i++)
        //        for (int j = 0; j < factor1.GetLength(1); j++)
        //            factor1[i, j] = random.Next();
        //    for (int i = 0; i < factor2.GetLength(0); i++)
        //        for (int j = 0; j < factor2.GetLength(1); j++)
        //            factor2[i, j] = random.Next();
        //    for (int i = 0; i < factor1.GetLength(0); i++)
        //        for (int j = 0; j < factor2.GetLength(1); j++)
        //        {
        //            product[i, j] = 0;
        //            for (int k = 0; k < d2; k++)
        //                product[i, j] += factor1[i, k] * factor2[k, j];
        //        }
        //    return;
        //}
        //protected void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    using (ZContext context = new ZContext())
        //    using (ZSocket receiveQueue = new ZSocket(context, ZSocketType.DEALER),
        //                   sendQueue = new ZSocket(context, ZSocketType.DEALER))
        //    {
        //        int filesToBeReceived = 0;
        //        // Initialize queues
        //        sendQueue.SendHighWatermark = 2;
        //        if (!String.IsNullOrWhiteSpace(SendQueueAddress))
        //            switch (Architecture)
        //            {
        //                case Architectures.SeparateQueues:
        //                    receiveQueue.ReceiveHighWatermark = RcvHwm;
        //                    sendQueue.Bind(SendQueueAddress);
        //                    break;
        //                case Architectures.SingleQueue:
        //                    sendQueue.Connect(SendQueueAddress);
        //                    receiveQueue.ReceiveHighWatermark = 2;
        //                    receiveQueue.IdentityString = this.GetType().Name;
        //                    break;
        //            }
        //        receiveQueue.Connect(ReceiveQueueAddress);
        //        Console.WriteLine(this.GetType().Name + " ready.");
        //        while (true)
        //        {
        //            if (this is PriceAggregator)
        //                System.Threading.Thread.Sleep(1000000);
        //            // Receive message.
        //            string messageString;
        //            using (ZFrame frame = receiveQueue.ReceiveMessage()[0])
        //            {
        //                messageString = frame.ReadString();
        //            }
        //            if (messageString.StartsWith("TotalFiles"))
        //                filesToBeReceived = Int32.Parse(messageString.Substring("TotalFiles".Length));
        //            else
        //            {
        //                XmlDocument xmlIn = new XmlDocument();
        //                xmlIn.LoadXml(messageString);
        //                FilesReceived++;
        //                FilesSent += CreateAndSendFiles(xmlIn, sendQueue);
        //                //--//Console.WriteLine("    Business: " + sentFiles + "/" + processedFiles);
        //            }
        //            if (FilesReceived == filesToBeReceived)
        //            {
        //                ZMessage messageTotalFiles = new ZMessage();
        //                if (Architecture == Architectures.SingleQueue)
        //                    switch (this.GetType().Name)
        //                    {
        //                        case "Technical":
        //                            messageTotalFiles.Add(new ZFrame("Business"));
        //                            break;
        //                        case "Business":
        //                            messageTotalFiles.Add(new ZFrame("PriceAggregator"));
        //                            break;
        //                        case "PriceAggregator":
        //                            messageTotalFiles.Add(new ZFrame("Resolution"));
        //                            break;
        //                        case "Resolution":
        //                            messageTotalFiles.Add(new ZFrame("Program"));
        //                            break;
        //                    }
        //                messageTotalFiles.Add(new ZFrame("TotalFiles" + FilesSent));
        //                sendQueue.SendMessage(messageTotalFiles);
        //                break;
        //            }
        //        }
        //        Console.WriteLine(this.GetType().Name + ": " + filesToBeReceived + " -> " + FilesSent);
        //    }

        //}

    }

}
